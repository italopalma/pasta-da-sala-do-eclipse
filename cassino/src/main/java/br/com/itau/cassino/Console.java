package br.com.itau.cassino;

public class Console {

	public void imprimeJogada(String valor, int pontuacao) {
		System.out.println("Sorteado: " + valor + " valendo " + pontuacao);
	}

	public void imprimePontuacao(CacaNiquel cacaNiquel, Valores valor1, Valores valor2, Valores valor3) {
		System.out.println("Pontuação Total: " + cacaNiquel.geraPontuacao(valor1, valor2, valor3));

	}

}
