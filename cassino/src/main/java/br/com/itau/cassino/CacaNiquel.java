package br.com.itau.cassino;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CacaNiquel {

	List<Valores> valores = new ArrayList<Valores>();
	int tamanho;

	public CacaNiquel() {

		for (Valores valor : Valores.values()) {
			valores.add(valor);
		}
		tamanho = valores.size();
	}
	
	public Valores geraValor() {
		 
		Random posicao = new Random();
		return valores.get(posicao.nextInt(tamanho));
		
	}
	
	public int geraPontuacao(Valores valor1, Valores valor2, Valores valor3) {
		
		if(valor1.toString().equals(Valores.Sete.toString()) &&
		   valor2.toString().equals(Valores.Sete.toString()) && 
		   valor3.toString().equals(Valores.Sete.toString()))
			return (valor1.getValor() + valor2.getValor() + valor3.getValor()) * 5;
		
		if(valor1.toString().equals(valor2.toString()) || 
		   valor1.toString().equals(valor3.toString()) || 
		   valor2.toString().equals(valor3.toString()))
			return (valor1.getValor() + valor2.getValor() + valor3.getValor()) * 2;
		
			
		return (valor1.getValor() + valor2.getValor() + valor3.getValor());
		
	}
}
