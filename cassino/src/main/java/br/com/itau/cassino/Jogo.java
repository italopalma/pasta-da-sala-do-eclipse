package br.com.itau.cassino;

public class Jogo {

	private CacaNiquel cacaNiquel;
	private Console console;

	public Jogo() {
		this.cacaNiquel = new CacaNiquel();
		this.console = new Console();
		
	}

	public void jogaJogo() {

	
		Valores valor1 = cacaNiquel.geraValor();
		console.imprimeJogada(valor1.toString(), valor1.getValor());
		Valores valor2 = cacaNiquel.geraValor();
		console.imprimeJogada(valor2.toString(), valor2.getValor());
		Valores valor3 = cacaNiquel.geraValor();
		console.imprimeJogada(valor3.toString(), valor3.getValor());

		console.imprimePontuacao(cacaNiquel, valor1, valor2, valor3);

	}

	public CacaNiquel getCacaNiquel() {
		return cacaNiquel;
	}

	public Console getConsole() {
		return console;
	}
}
