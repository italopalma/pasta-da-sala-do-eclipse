package br.com.itau.cassino;

public enum Valores {
	Laranja(10),
	Banana(20),
	Limão(50),
	Sete(100);
	
	private int pontuacao;
	
	private Valores(int pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	public int getValor() {
		return pontuacao;
	}
	
/*	public String toString() {
		return "Sorteado " + Valores.values() + " valendo ";
	}*/

}
