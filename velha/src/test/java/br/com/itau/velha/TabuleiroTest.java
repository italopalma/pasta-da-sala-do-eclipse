package br.com.itau.velha;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TabuleiroTest {
	
	Tabuleiro tabuleiro;
	
	@Before
	public void Incializa() {
		tabuleiro = new Tabuleiro();
	}

	@Test
	public void deveInicializarOTabuleiro() {
		
		
		Simbolo[][] casas = tabuleiro.getCasas();
		
		for (int i = 0;  i < casas.length; i++) {
			for (int j = 0; j < casas.length; j++) {
				Assert.assertEquals(Simbolo.Vazio, casas[i][j]);
			}
		}
	}
		
	@Test
	public void deveFazerUmaJogada() throws CasaInexistente, CasaPreenchida {
				
		tabuleiro.jogar(0, 0, Simbolo.O);
		
		Assert.assertEquals(Simbolo.O, tabuleiro.getCasas()[0][0]);
	}
	
	@Test(expected = CasaInexistente.class)
	public void deveLancarExcessaoQuandoJogarEmCasaInexistente() throws CasaInexistente{
		
		
		
	}
		
	
		
		
	

}
