package br.com.itau.velha;

public class Tabuleiro {
	private Simbolo[][] casas;
	
	public Tabuleiro() {
		casas = new Simbolo[3][3];
		
		for (int i = 0; i < casas.length; i++) {
			for (int j = 0; j < casas.length; j++) {
				casas[i][j] = Simbolo.Vazio;
			}
		}
	}
	
	public Simbolo[][] getCasas(){
		return casas;
	}
	
	
	public void jogar(int x, int y, Simbolo simbolo) throws CasaInexistente, CasaPreenchida{
	
	if(x > 2 || x < 0 || y > 2 || y < 0) {
		throw new CasaInexistente();
	}
	
	if(casas[x][y] != Simbolo.Vazio) {
		throw new CasaPreenchida();
	}
		
		
	casas[x][y] = Simbolo.O;
		
	}

}
