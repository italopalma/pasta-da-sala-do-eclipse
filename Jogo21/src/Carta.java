
public class Carta {
	
	private String Naipe;
	private String Nome;
	private int pontuacao;
	private static int qtdCartas;
	
	public Carta(String Naipe, String Nome, int pontuacao) {
		this.Naipe = Naipe;
		this.Nome = Nome;
		this.pontuacao = pontuacao;
		qtdCartas++;
	}
	
	public String toString() {
		return Naipe + " " + Nome + " " + pontuacao;
	}

}
