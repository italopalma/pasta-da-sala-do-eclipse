package com.itau.geometria;

import java.util.List;

public class Retangulo extends Forma {

//	public boolean validaLados(List<Double> lados) {
//		if(lados.size() == 2) {
//			return true;
//		}
//		return false;
//	}
	
	public Retangulo(List<Double> lados, int ladosEsperados) {
		super(lados, 2);
	}
	
	@Override
	public double calculaArea(List<Double> lados) {
		// TODO Auto-generated method stub
		return lados.get(0) * lados.get(1);
	}

}
