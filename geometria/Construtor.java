package com.itau.geometria;

import java.util.List;

public class Construtor {
	
	List<Double> lados;
	
	public Construtor(List<Double> lados) {
		
		this.lados = lados;
		
	}
	
	public void identificaForma() {
		switch(lados.size()) {
	     case 1:
	       Circulo circulo = new Circulo(lados, 1);
	       
	       break;
	     case 2:
	       Retangulo retangulo = new Retangulo(lados, 2);
	       
	       break;
	     case 3:
	    	 Triangulo triangulo = new Triangulo(lados, 3);
	    
	       break;
	     //default: throw new Exception("Não implementado");
	    }
	}
	
	public void calculos(Forma forma) {
	       forma.calculaArea(lados);
	}

}
