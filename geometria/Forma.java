package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

public abstract class Forma {
	
	public Forma(List<Double> lados, int ladosEsperados) {
		validaLados(lados, ladosEsperados);
	
	}

	public abstract double calculaArea(List<Double> lados);
		
	protected void validaLados(List<Double> lados, int ladosEsperados) {
		if(lados.size() != ladosEsperados) {
			throw new RuntimeException(" Quantidade de Lados Informada Incorreta!");
			
		}
	
	}
}
