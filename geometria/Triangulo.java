package com.itau.geometria;

import java.util.List;

public class Triangulo extends Forma {
	
//	public boolean validaLados(List<Double> lados) {
//		if(lados.size() == 3) {
//			return true;
//		}
//		return false;
//	}

	public Triangulo(List<Double> lados, int ladosEsperados) {
		super(lados, ladosEsperados);
	}
	
	public boolean validaLadosTriangulo(List<Double> lados) {

		if ((lados.get(0) + lados.get(1) > lados.get(2)) && (lados.get(0) + lados.get(2) > lados.get(1))
				&& (lados.get(1) + lados.get(2) > lados.get(0))) {

			return true;
		}
		
		return false;
	}

	@Override
	public double calculaArea(List<Double> lados) {
		// TODO Auto-generated method stub
		double s = (lados.get(0) + lados.get(1) + lados.get(2)) / 2;
		return (Math.sqrt(s * (s - lados.get(0)) * (s - lados.get(1)) * (s - lados.get(2))));
	}

}
