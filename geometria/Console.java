package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Console {

	public List interageUsuario() {

		Scanner scanner = new Scanner(System.in);

		List<Double> lados = new ArrayList<>();

		double entrada = -1.0;

		while (true) {
			System.out.println("Digite um lado ou digite zero para encerrar: ");

			String d = scanner.nextLine();
			entrada = Double.parseDouble(d);

			if (entrada > 0) {
				lados.add(entrada);
			} else {
				break;
			}
		}

		scanner.close();
		
		return lados;
	}

}
