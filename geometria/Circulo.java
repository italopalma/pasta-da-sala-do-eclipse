package com.itau.geometria;

import java.util.List;


public class Circulo extends Forma {
	
/*	public boolean validaLados(List<Double> lados) {
		if(lados.size() == 1) {
			return true;
		}
		return false;
	}*/
	
	public Circulo(List<Double> lados, int ladosEsperados) {
		super(lados, 1);
	}
	
	@Override
	public double calculaArea(List<Double> lados) {
		// TODO Auto-generated method stub
		return Math.pow(lados.get(0), 2) * Math.PI;
	}

}
