package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
	
  public static void main(String[] args) throws java.lang.Exception {
    Scanner scanner = new Scanner(System.in);

    //Esse bloco seria recuperar as intenções do usuário e retornar uma lista
    List<Double> lados = new ArrayList<>();
    
    double entrada = -1.0;
    
    while (true) {
      System.out.println("Digite um lado ou digite zero para encerrar: ");
      
      String d = scanner.nextLine();
      entrada = Double.parseDouble(d);
      
      if(entrada > 0) {
        lados.add(entrada);
      }else {
        break;
      }
    }
    
    scanner.close();
    
    //Aqui podemos criar a classe Forma e então com a Herança suas formas filhas
    switch(lados.size()) {
     case 1:
       System.out.println(Math.pow(lados.get(0), 2) * Math.PI);
       break;
     case 2:
       System.out.println(lados.get(0) * lados.get(1));
       break;
     case 3:
       if(lados.get(0) + lados.get(1) > lados.get(2) && 
           (lados.get(0) + lados.get(2)) > lados.get(1) && 
           (lados.get(1) + lados.get(2)) > lados.get(0)){
             double s = (lados.get(0) + lados.get(1) + lados.get(2)) / 2;       
             System.out.println(Math.sqrt(s * (s - lados.get(0)) * (s - lados.get(1)) * (s - lados.get(2))));
           }else {
             
             throw new Exception("Triângulo Inválido");
           }
       break;
     default: throw new Exception("Não implementado");
    }
  }

}
