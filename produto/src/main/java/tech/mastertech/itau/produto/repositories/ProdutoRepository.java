package tech.mastertech.itau.produto.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

	//Ao criar o método abaixo na Interface com exatamente o mesmo nome do método que busca pelo ID,
	//fazemos com que automaticamente seja implementada a busca pela Categoria
	public Iterable<Produto> findAllByCategoria(Categoria categoria);
	
	public Optional<Produto> findByCategoriaAndValor(Categoria categoria, double valor);
	
}
