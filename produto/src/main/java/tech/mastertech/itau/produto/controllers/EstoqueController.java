package tech.mastertech.itau.produto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.dto.Produto;
import tech.mastertech.itau.produto.services.ProdutoService;

@RestController
@RequestMapping("/estoque") // *direcionador da URI para classe
public class EstoqueController {

	//* implementar um endpoint para recuperar produto por ID
	//* implementar um endpoint para atualizar o valor de um produto
	
	// * Injeção de Dependências. Direciona a responsabilidade de instanciar para o
	// Spring.
	// * Mantém a mesma instância desse objeto durante toda a execução da aplicação
	@Autowired
	private ProdutoService produtoService;

	@GetMapping("/produtos")
	public Iterable<Produto> getProdutos() {
		return produtoService.getProdutos();
	}

	@GetMapping("/produto/{id}")
	public Produto getProdutoId(@PathVariable int id) {
	
		Produto retorno = produtoService.getProdutoId(id);
		
		if(retorno == null) 
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		
		
			return retorno;
	}
	
	@PostMapping("/produto")
	public Produto setProduto(@RequestBody Produto produto) {
		return produtoService.setProduto(produto);
	}
	
/*	@PatchMapping("/produto")
	public Produto atualizaValorProduto(@RequestParam int id, double novoValor) {

		return produtoService.atualizaValorProduto(id, novoValor);
	}*/

	/*
	 * @GetMapping("/produtos") public List<Produto> getProdutos(){ Produto p1 = new
	 * Produto(); Produto p2 = new Produto();
	 * 
	 * p1.setId(1); p1.setNome("Corote"); p1.setValor(3.50);
	 * p1.setCategoria(Categoria.ALCOOL);
	 * 
	 * p2.setId(2); p2.setNome("Shampoo"); p2.setValor(40.0);
	 * p2.setCategoria(Categoria.LIMPEZA);
	 * 
	 * List<Produto> produtos = new ArrayList<>();
	 * 
	 * produtos.add(p1); produtos.add(p2);
	 * 
	 * return produtos;
	 * 
	 * }
	 */

	
/*	  @GetMapping("/hello") //*quem direciona esse mapeamento do método é o get do cliente
	  public String helloEstoque() {
	  
	  return "Hello Estoque";
	  
	  }*/
	  
	/* @GetMapping("/repetir/{palavra}") //*esse mapeamento recepciona a variável
	 * após a / public String repetir(@PathVariable String palavra) {
	 * 
	 * return "A palavra é " + palavra;
	 * 
	 * }
	 * 
	 * //*esse mapeamento se diferencia do de cima por conta do parâmetro recebido,
	 * isso faz com que sejam distintos
	 * 
	 * @GetMapping("/repetir") //* esse mapeamento exige que as variáveis sejam
	 * passadas no método com ?var="valor" e & para concatenar variáveis public
	 * String repetirParam(@RequestParam String palavra, int numero) {
	 * 
	 * return "A palavra no Header é " + palavra + " " + numero;
	 * 
	 * }
	 * 
	 * //* nesse caso estamos acessando o mesmo endpoint(/repetir), mas temos
	 * métodos diferentes de acesso. Aqui éxecutado um Post. No de cima era um Get.
	 * 
	 * @PostMapping("/repetir") //* esse mapeamento exige public String
	 * repetirBody(@RequestBody String palavra) {
	 * 
	 * return "A palavra no Body é " + palavra;
	 * 
	 * }
	 */

}
