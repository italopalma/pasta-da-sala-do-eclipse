package tech.mastertech.itau.produto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;
import tech.mastertech.itau.produto.repositories.ProdutoRepository;

//* essa anotação informa que a classe é um Serviço
@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	//*private List<Produto> produtos;
	private int novoId = 0;

	public ProdutoService() {
		novoId = 1;
	}

	public Iterable<Produto> getProdutos() {

		return produtoRepository.findAll();

	}

	public Produto setProduto(Produto produto) {
//		produto.setId(novoId);
//		novoId++;
		produtoRepository.save(produto);
		return produto;
	}

	public Produto getProdutoId(int id) {

//		for (Produto produto : produtos) {
//			if (produto.getId() == id)
//				return produto;
		
		
		Optional<Produto> produto = produtoRepository.findById(id);
		
		if(produto.isPresent()) {
			return produto.get();
		}
	
		return null;
	}
	
	public Produto atualizaValorProduto(int id, double novoValor) {
		
		Produto produto = getProdutoId(id);
		
		produto.setValor(novoValor);
		
		return produtoRepository.save(produto);
		
	}

}
