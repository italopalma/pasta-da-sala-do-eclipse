package com.example.seguranca.security;



import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtTokenProvider {
	
	private final String chaveSecreta = "aVerySecretKey";
	
	private final int duracaoEmMiliSegundos = 100000000;
	
	public String CriarToken(String userId) {
	
		
		Claims claims = Jwts.claims();
		
		claims.put("userId", userId);
		
		Date agora = new Date();
		
		Date exp = new Date(agora.getTime() + duracaoEmMiliSegundos);
		
		return Jwts.builder()
				.setClaims(claims)
				.setExpiration(exp)
				.signWith(SignatureAlgorithm.HS256, chaveSecreta)
				.compact();
		
	}
	
	public String validarToken(String token) {
		
		Jws<Claims> jwsClaims = Jwts.parser()
				.setSigningKey(chaveSecreta)
				.parseClaimsJws(token);
		
		return jwsClaims.getBody().get("userId", String.class);
	}

}
