package com.example.seguranca.controllers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExemploController {
	
	@GetMapping("/livre")
	public String Livre() {
		return "endpoint sem autenticacao";
	}
	
	@GetMapping("/admin")
	public String Admin() {
		return "endpoint autenticado";
	}

}
