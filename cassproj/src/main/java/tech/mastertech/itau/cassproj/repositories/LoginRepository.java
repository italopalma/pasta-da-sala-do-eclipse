package tech.mastertech.itau.cassproj.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.Login;

public interface LoginRepository extends CrudRepository<Login, String>{
	
	public Optional<Login> findByUsuario(String usuario);

}
