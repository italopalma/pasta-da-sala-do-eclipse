package tech.mastertech.itau.cassproj.models;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class PinUsuario {

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String usuario;
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private UUID dataPostagem;
	@NotBlank
	private String categoria;
	@NotBlank
	private String descricao;
	@NotBlank
	private String imagem;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public UUID getDataPostagem() {
		return dataPostagem;
	}

	public void setDataPostagem(UUID dataPostagem) {
		this.dataPostagem = dataPostagem;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

}
