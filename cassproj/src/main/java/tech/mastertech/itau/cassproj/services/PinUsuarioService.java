package tech.mastertech.itau.cassproj.services;

import java.security.Principal;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;

import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.repositories.PinCategoriaRepository;
import tech.mastertech.itau.cassproj.repositories.PinUsuarioRepository;

@Service
public class PinUsuarioService {

	@Autowired
	PinUsuarioRepository pinUsuarioRepository;
	
	@Autowired
	PinCategoriaRepository pinCategoriaRepository;

	public Iterable<PinUsuario> buscaPins(String usuario) {

		return pinUsuarioRepository.findByUsuario(usuario);

	}

	public PinUsuario buscaPin(String usuario, UUID datapostagem) {

		return pinUsuarioRepository.findByUsuarioAndDataPostagem(usuario, datapostagem).get();

	}

	public PinUsuario cadastraPin(PinUsuario pinUsuario, Principal principal) {
	
		PinCategoria pinCategoria = new PinCategoria();
		
		pinUsuario.setUsuario(principal.getName());
		pinCategoria.setUsuario(principal.getName());
		
		pinUsuario.setDataPostagem(UUIDs.timeBased());
		pinCategoria.setDataPostagem(pinUsuario.getDataPostagem());
		
		pinCategoria.setCategoria(pinUsuario.getCategoria());
		pinCategoria.setDescricao(pinUsuario.getDescricao());
		pinCategoria.setImagem(pinUsuario.getImagem());
		
		pinCategoriaRepository.save(pinCategoria);
		
		return pinUsuarioRepository.save(pinUsuario);

	}

	public void removePin(PinUsuario pinUsuario) {

		pinUsuarioRepository.delete(pinUsuario);

	}

}
