package tech.mastertech.itau.cassproj.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.repositories.PinCategoriaRepository;

@Service
public class PinCategoriaService {
	
	@Autowired
	PinCategoriaRepository pinCategoriaRepository;
	
	public Iterable<PinCategoria> buscaCategoria(String categoria){
		return pinCategoriaRepository.findByCategoria(categoria);
	}
	

}
