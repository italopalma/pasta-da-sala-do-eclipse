package tech.mastertech.itau.cassproj.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tech.mastertech.itau.cassproj.models.PinUsuario;

@Repository
public interface PinUsuarioRepository extends CrudRepository<PinUsuario, String>{
	
	public Iterable<PinUsuario> findByUsuario(String usuario);
	
	public Optional<PinUsuario> findByUsuarioAndDataPostagem(String usuario, UUID dataPostagem);
	
	

}

