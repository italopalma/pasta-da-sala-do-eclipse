package tech.mastertech.itau.cassproj.dto;

public class LoginResposta {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
