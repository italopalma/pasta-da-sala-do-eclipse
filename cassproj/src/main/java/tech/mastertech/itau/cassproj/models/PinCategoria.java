package tech.mastertech.itau.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class PinCategoria {
	
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String categoria;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID dataPostagem;
	private String usuario;
	private String descricao;
	private String imagem;
	
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public UUID getDataPostagem() {
		return dataPostagem;
	}
	public void setDataPostagem(UUID dataPostagem) {
		this.dataPostagem = dataPostagem;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

}
