package tech.mastertech.itau.cassproj.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.services.PinCategoriaService;
import tech.mastertech.itau.cassproj.services.PinUsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	PinUsuarioService pinUsuarioService;
	
	@Autowired
	PinCategoriaService pinCategoriaService;
	
	@PostMapping("/pin")
	public PinUsuario cadastraPin(@RequestBody PinUsuario pinUsuario, Principal principal) {
		return pinUsuarioService.cadastraPin(pinUsuario, principal);
	}
	
	@DeleteMapping("/pin")
	public void deletaPin(@RequestBody PinUsuario pinUsuario) {
		pinUsuarioService.removePin(pinUsuario);
	}
	
	@GetMapping("/listapin")
	public Iterable<PinUsuario> buscaPinsCategoria(Principal principal){
		
		return pinUsuarioService.buscaPins(principal.getName());
		
	}

	@GetMapping("/pin")
	public PinUsuario buscaPin(@RequestBody PinUsuario pinUsuario) {
		
		return pinUsuarioService.buscaPin(
				pinUsuario.getUsuario(), pinUsuario.getDataPostagem());
	}
	
	@GetMapping("/listacategorias/")
	public Iterable<PinCategoria> buscaPins(@PathVariable String categoria){
		
		return pinCategoriaService.buscaCategoria(categoria);
		
	}
	
}
