
public enum Cartas {
	
	A(1),
	B(2),
	C(3),
	D(4);
	
	private int valor;
	
	private Cartas (int valor) {
		this.valor = valor;
	}
	
	public int retornaValor() {
		return valor;
	}
	
	//public String toString() {
	//	return "Enum";
	//}
}
