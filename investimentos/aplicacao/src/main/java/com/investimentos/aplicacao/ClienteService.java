package com.investimentos.aplicacao;

import org.springframework.web.client.RestTemplate;

public class ClienteService {
	
	RestTemplate restTemplate = new RestTemplate();
	
	public Cliente buscar(String id) {
		return restTemplate.getForObject("http://localhost:8081/" + id, Cliente.class);
	}

}
